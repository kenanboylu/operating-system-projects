#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
/* Above commands are library */

#define MAX_LINE 80 /* 80 chars per line, per command, should be enough. */
#define MAX_CHAR 32
#define PATH_MAX 4096
typedef char *String; /* define String struct */
extern char **environ;/* system envirement array */

char * prompt = "333sh:" ; /* get input line prompt */
char * cmdline;            /* command line string */
char inputBuffer[MAX_LINE];/* put to buffer inputBuffer array*/
char *inputTemp;           /* input temp */

char pipe1[64];       /* First pipe command array without splitted. */
char pipe2[64];       /* Second pipe command array without splitted.*/
String strPipe1[10];  /* First pipe splitted command ,string array*/ 
String strPipe2[10];  /* Second pipe splitted command,string array*/ 


int cnt = 0;          /* count for printing history */ 
char *delim = " ><&%!\n";/* delim string */

struct history {   /* keeping all commands in to history */
    char *command; /* keep comand name */
    int num;       /* keep command number */
};

struct history History[10]; /* struct array size 10 */
static volatile sig_atomic_t doneflag = 0; /* <Control C> inital doneflag signal */
/* ARGSUSED */
static void setdoneflag(int signo) { /* set doneflag if <control C> pressed */
   doneflag = 1;                     /* set doneflag 1 */
}
/*All pipe methods */
void splitPipe();
void splitPipe1(String string, String delim);
void splitPipe2(char *string, char *delim);
void pipeExecution();
/* end of pipe methos */

int setup(){ /* setup function include ssytem and build in command */
 

int argNum;         /* arguments array length */
char *args[64];     /* arguments array length */                                  
char **next = args; /* next pointer pointer */                                

   
           printf("%s",prompt);                        /* print "333sh:" input get line */                           
           cmdline = fgets(inputBuffer,MAX_LINE,stdin); /* get input commmand line */
           char *inputTemp = (char *)malloc(strlen(inputBuffer) + 1); /* puts inputBuffer to inpuTemp string */
           strcpy(inputTemp, inputBuffer); /* copy to inputTemp */
           
	   char *temp = strtok (inputTemp, " \n"); /* divide input to commands */
           argNum=0;      
	   while (temp != NULL){  /* while temp not null */               
           argNum++; /* arguments numbers */                                       
           *next++ = temp;        /* assign command to next pointer. */                          
          // printf("%s\n", temp); 
           temp = strtok(NULL, " \n"); /* get input commmand line */  

	  }
          *next = NULL;
          int i; 
        
        int b=0;
        while( b < argNum){
        if (!strcmp(args[b],"&")) 
        args[b]=NULL;
        b++;
        }

if (args[0]!=NULL) { /* if arguments is not null  */                           
   pid_t childpid;   /* define chidpid */
 
if (isPIPE()) {      /* start pipe command execution */ 
            splitPipe(); /* Firstly , split inputBuffer for "|" character ,divide pipe1 and pipe2 string */
            splitPipe1(pipe1, delim); /* split pipe1 with delim string */ 
            splitPipe2(pipe2, delim); /* split pipe2 with delim string */
            if ((childpid = fork()) == 0) {  /* create child process */
              pipeExecution();               /* go pipeExecution menthod */
            } else { 
                wait(NULL);                  /* else wait child creation */
            } 
           
}
else if(isBltIN(args)) { /*If command is build in command.*/

      if (!strcmp(args[0], "cd")) { /*if args[0] is "cd" */
                    i = 0;
                int t = 0;
                    while (args[i] != NULL) {   
                         t=chdir(args[i]);    /* go directory in args */
                        i++;
                    }
                if (t != 0) /*if directory name is not exist, give error */
                printf("Warning: %s directory name is not exist.\n",args[1]);
                else  /* you can control directory is changed or not */
                printf("Directory is changed..try pwd command for current directory\n");

                } else if (!strcmp("print", args[0])) { /* print command */
                       isPrint(args);    /* go isPrint medhod and print all envirements */

                } else if (!strcmp(args[0], "set")) {  /* set varname = somevalue */
                       setEnv(args);     /*go setEnv method and set varname with somevalue */

                } else if (!strcmp("where", args[0])) {
                      findpathEX(args[1]);
                } else if (!strcmp("path", args[0])) {
                     isPATHCommand(args);
                } else if (!strcmp("clr", args[0])) { /* clr command */
                       system("clear");    /* use system command */

                } else if (!strcmp("exit", args[0])) { /* exit command */
                       exit(0);           /* exit (0) */
                }
 } /*buil-in command end */

else {  /* start system command */
                             
     if(isBackground()){  /* Background Processes BURAYA TEKRAR BAK */ 
   	if((childpid = fork()) == 0){  /* Child Process */
  	    if (isPATH()) {  /* If path exist */
                 if(argNum==2)
      	    	 execl(args[1], args[0], NULL); /* Run execl function */
            	 else if(argNum==3)
             	 execl(args[2], args[0],args[1],NULL); /* Run execl function */
             	 else if(argNum==4)
      		 execl(args[3], args[0], args[1],args[2], NULL); /* Run execl function */
                 perror("Child failed to execl the command"); /* error */
   		 }
 		
		 else {  /* If path does not exist */                                           
      		 execvp(args[0],args);  /* Run execvp function */                       
     		 perror("Child failed to execvp the command"); /* error */   
 		 }
        }//else 
       // printf("Parent is not running"); /*Parent Process */
}
      else { /* Foreground Processes */                          
        if((childpid = fork()) == 0){ /* Child Proces */ 
            if (isPATH()){ /* If path exist */
             if(argNum==2)
      	     execl(args[1], args[0], NULL); /* Run execl function */
             else if(argNum==3)
             execl(args[2], args[0],args[1],NULL); /* Run execl function */
             else if(argNum==4)
             execl(args[3], args[0],args[1],args[2],NULL); /* Run execl function */
             perror("Child failed to execl the command"); /* error */
	     } 
             else { /* If path does not exist */        
             execvp(args[0], args);  /* Run execvp function */
             perror("Child failed to execvp the command"); /* error */
             }
       }else  if (childpid != wait(NULL)) {/* Parent proceess wait for child process */         
               perror("Parent failed to wait");                             
  	      }
   } 
}
}
else /* arg[0] is null thats mean there is not any input */
printf("Warning: Give a input....\n"); /* give error not any input */
free(inputTemp);  /* make empty inputemp */
inputTemp = NULL;

} /* END OF SETUP() */

 
int main (int argc, char ** argv)        /* main method */
{
     
   int count = 0; /** count is incrementer, if any command is exucuted  */
   
   struct sigaction act; /* is assigned the previous action associated with the signal */

   act.sa_handler = setdoneflag;         /* set up signal handler */
   act.sa_flags = 0;                     /* is assigned special flags and options */
   if ((sigemptyset(&act.sa_mask) == -1) ||
       (sigaction(SIGINT, &act, NULL) == -1)) { /* function should restore the default action for the signal*/
      perror("Failed to set SIGINT handler");   /* error */
      return 1;
   }

  char *first;
    first = NULL;
    first= malloc(sizeof (char) *128);

   // int cnt = 0;
int j=0;
    for (j = 0; j < 10; j++) {
        History[j].command = NULL;
        History[j].num = 0;
       
    } 
while (!doneflag){

if ((History[cnt].command == NULL)&&(cnt <= 10)) { /*if history command null and cnt small than 10 */
            //inputBuffer boyutu kadar memory ayır
            History[cnt].command = malloc(sizeof (char) *strlen(inputBuffer)); 
            strcpy(History[cnt].command, inputBuffer); /* copy inputBuffer to History command.*/
            History[cnt].num = (cnt + 1);     /*  History[].num assign.*/
            cnt++;                            /* cnt inrement */
            
        }

        if (cnt > 10) {      /* if cnt greater than 10 */
            strcpy(first, History[0].command); /* assing History[0].command to firts command.*/
            for (j = 0; j < 9; j++)
            strcpy(History[j].command, History[j + 1].command); /* assign command to previous command*/
            strcpy(History[9].command, inputBuffer);            /* assign last command to History[9]*/
        }
       
count++;
setup();

}
if (count > 0)  /* if count greater than zero, control C signal exist */
printHistory();	/* go printHistory  and print recently 10 commands */		
return 0; 
}

int printHistory(){ /* printHistory  method*/
int k=1;
//printf("cnt=%d\n",cnt);
while (k < cnt){
 printf("%d history:%s",k,History[k].command); /*print History command */
 k++;
}
return 1;
}

int isBackground() {  /* Bacground Process */
    char *p;
    p = strchr(inputBuffer, '&');
    if (p != NULL) {
        return 1;
    }
    return 0;
}
int isPATH() {     /* If command line contains path */
    char *p;
    p = strchr(inputBuffer, '/');
    if (p != NULL) {
        return 1;
    }
    return 0;
}

int isPIPE() {  /* PIPE commands */
    char *p;
    p = strchr(inputBuffer, '|');
    if (p != NULL) {
        return 1;
    }
    return 0;
}   
int isBltIN(char *args[]) {     /* Buld-in commands */
    if (!strcmp("cd", args[0])) /* compare if args[0] equals cd */
        return 1;
    else if (!strcmp("clr", args[0]))
        return 1;
    else if (!strcmp("print", args[0]))
        return 1;
    else if (!strcmp("where", args[0]))
        return 1;
    else if (!strcmp("set", args[0]))
        return 1;
    else if (!strcmp("path", args[0]))
        return 1;
    else if (!strcmp("exit", args[0]))
        return 1;
    else
        return 0;
}
void strupper(char *s) { /* string convert uppercase.*/
    while (*s) {
        if ((*s >= 'a' ) && (*s <= 'z')) *s -= ('a'-'A');
        s++;
    }
}

int isPrint(char *args[]){ /* isPrint method */
char *str2;
char *str=args[1];
char *varname;
int k;
          if (str==NULL){ /*str is null  print all envirements */
             if (environ != NULL) /* environment not null */
	     for (k = 0; environ[k] != NULL; k++){  
             str2=environ[k];            /*copy environ array */
             printf("%s\n", str2); /* print all envirements */
             }                     
          }        
          else{          /*if print varname envirement */
              strupper(str); /*convert uppercase */
              varname = getenv(str); /* get envirement */
              printf("%s=%s\n",str, varname); /*print varname envirement */

          }
	  return 0;
}

int checkifexecutable(const char *filename) /* Check file executable method */
{
     int result;
     struct stat statinfo; 
     
     result = stat(filename, &statinfo);
     if (result < 0)
       return 0;
     if (!S_ISREG(statinfo.st_mode))
      return 0;

     if (statinfo.st_uid == geteuid()) 
     return statinfo.st_mode & S_IXUSR; 
     if (statinfo.st_gid == getegid())
     return statinfo.st_mode & S_IXGRP; 
     return statinfo.st_mode & S_IXOTH; 
}


int findpathEX(const char *exe)
{
     char path[PATH_MAX+1]; /* the path is available space for found path.*/
     char *begin;           /* begin is start searching path */  
     char *end;             /* end is  end of path */ 
     int stop, found;       /* found is 1,if file is executable */
     int len;

     begin = getenv("PATH");
     stop = 0; found = 0;
     do {
	  end = strchr(begin, ':');
          printf("max=%d\n",PATH_MAX);
	  if (end == NULL) {  /* if path finished */ 
	       stop = 1;      /* stop =1 */
	       strncpy(path, begin, PATH_MAX); /* copy begin to path array */
	       len = strlen(path); /* measure path length */
	  } else {
	       strncpy(path, begin, end - begin); /* copy begin to path array */
	       path[end - begin] = '\0';   /* assign remainder part is null */
	       len = end - begin;  /* measure path length */
	  } 
	  if (path[len - 1] != '/')
          strncat(path, "/", 1);
	  strncat(path, exe, PATH_MAX - len); /* append file to path */
	  found = checkifexecutable(path);    /* check this file executable or not */
          
	  if (!stop)
          begin = end + 1;
     } while (!stop && !found); 
	if(found==1)    /*if file exexutable print path */
        printf("%s\n",path);
        else
        printf("NOT FOUND\n");  /*else give error */
} 

int isPATHCommand(char *args[]){ /* isPrint method */

char *str=args[1];  /* str is first argument + or - */
char *str2=args[2]; /* str is adding or removing path name */
char *path;         /* recent path */
char *pathname;
char *pathname2;
char pathname3[PATH_MAX];
String str4[15];
String str5[15];
String copySTR = NULL;
String token = NULL;
int k;
int i = 0;  
 
    path = getenv("PATH");     /* get envirement */
    /* create area for copying path */
    copySTR = malloc(sizeof (char) * strlen(path) + 1);
    /* in order to make sure that copy string is safe */
    strcpy(copySTR, path);
    token = strtok(copySTR, ":"); /* cutting strings acording to ":"
    i = 0;
    /* keep last entry NULL termindated always */
    while ((i < (MAX_CHAR - 1)) && (token != NULL)) {
        str4[i] = malloc(sizeof (char) * strlen(token) + 1);
        if (str4[i] != NULL) {
            strcpy(str4[i], token);
            i++;
            token = strtok(NULL, ":");
            
        }
    }        
          if (str==NULL){             /*str is null  print all path (no arguments) */
             int j=0;
              printf("Path List:\n");
             for (j=0; j<i; j++)
             printf("%d=>%s\n",j, str4[j]); /* print all environments */
                                        
          } else if(!strcmp(str,"+")){   /* if command is adding(+) */
              pathname = getenv("PATH");  /* get environment */
              strcat(pathname,":");       /* add new path name with ":" */
              strcat(pathname,str2);
              setenv("PATH",pathname,20); /* set environment variable varname to somevalue */
                                           
          } else if(!strcmp(str,"-")){   /* if command is adding(+) */
               pathname = getenv("PATH"); /* get envirement */
               int k=0;
               int h=0;
               for (k=0; k<i; k++){
               if(strcmp(str4[k],str2) != 0){ /* if pathname (str2) is not in path list */
               str5[h]=str4[k];               /* copy str4 string array another str5 string array */
               h++;
	       }
               }
               
               int j=0;
               for (j=0; j<h; j++) {
               //pathname3 = getenv("SHLVL");
               pathname2=(char*)str5[j]; /* print all envirements */
               strncat(pathname3,":",PATH_MAX);     /* add ":" to varname3*/
               strncat(pathname3,pathname2,PATH_MAX); /* add varname2 to varname3*/
	       }
               setenv("PATH",pathname3,20); /* set environment variable varname to somevalue */
               
          }

	  return 0;
}


int setEnv(char *args[])  /* setEnv method */
{
char *varname=args[1];
char *somevalue=args[3];
   strupper(varname);
   setenv(varname,somevalue,20); /* set environment variable varname to somevalue */
   return 0;
}




void splitPipe() {
int i = 0, counter = 0, k = 0;
    while (inputBuffer[i] != NULL) {
        if (inputBuffer[i] == '|') { /* if inputBuffer has '|' character*/
            counter++;               /* inrement counter */
        }

        if (!counter) {
            pipe1[i] = inputBuffer[i];   /* first pipe command */
        }

        if (counter) { 
            pipe2[k] = inputBuffer[i+1]; /* second pipe command */
            k++;
        }
        i++;
    }
}

void splitPipe1(String string, String delim) {

    String copySTR = NULL;
    String token = NULL;
    int i = 0;
    /* create area for copying string */
    copySTR = malloc(sizeof (char) * strlen(string) + 1);
    /* in order to make sure that copy string is safe */
    strcpy(copySTR, string);
    token = strtok(copySTR, delim);
    i = 0;
    /* keep last entry NULL termindated always */
    while ((i < (MAX_CHAR - 1)) && (token != NULL)) {
        strPipe1[i] = malloc(sizeof (char) * strlen(token) + 1);
        if (strPipe1[i] != NULL) {
            strcpy(strPipe1[i], token);
            i++;
            token = strtok(NULL, delim);
            //            printf("%s",token);
        }
    }
}

void splitPipe2(char *string, char *delim) {
    String copySTR = NULL;
    String token = NULL;
    int i = 0;
    /* create area for copying string */
    copySTR = malloc(sizeof (char) * strlen(string) + 1);
    /* in order to make sure that copy string is safe */
    strcpy(copySTR, string);
  
    token = strtok(copySTR, delim);
    i = 0;
    /* keep last entry NULL termindated always */
    while ((i < (MAX_CHAR - 1)) && (token != NULL)) {
        strPipe2[i] = malloc(sizeof (char) * strlen(token) + 1);
        if (strPipe2[i] != NULL) {
            strcpy(strPipe2[i], token);
            i++;
            token = strtok(NULL, delim);
            //            
        }
    }
     
}
void pipeExecution() {
    pid_t childpid;
    int fd[2];

    if ((pipe(fd) == -1) || ((childpid = fork()) == -1)) {
        perror("Failed to setup pipeline");
    }

    if (childpid == 0) { /*for instance that ls is the child */
        if (dup2(fd[1], STDOUT_FILENO) == -1) /* fd[1] output of ls command*/
            perror("Failed to redirect stdout of ls");
        else if ((close(fd[0]) == -1) || (close(fd[1]) == -1))
            perror("Failed to close extra pipeline descriptors on ls");
        else {
            execvp(strPipe1[0], strPipe1);/* execvp pipe1 command */
            perror("Failed to exec ls");
        }
    }

    if (dup2(fd[0], STDIN_FILENO) == -1) /* sort is the parent */ /* fd[0] output of sort command */
        perror("Failed to redirect stdin of sort"); 
    else if ((close(fd[0]) == -1) || (close(fd[1]) == -1)) 
        perror("Failed to close extra pipeline file descriptors on sort");
    else {
        execvp(strPipe2[0], strPipe2);/* execvp pipe2 command */
        perror("Failed to exec sort");
    }
}
