echo  #*********blank****
l1=0
if test $# = 0            #if it not exist input arguments
then

for file in *; do         #visit all files
    l=`grep -c "." $file` #find line number in files
    l1=`expr $l1 + $l`
done
    echo "Total is $l1."  #Total line in current directory.
    exit 1
fi


f=`find $pwd "$1" | awk -F"." '{print $NF}'` #convert first arg. to extention 
index=0
for i in $*;
do
f2=`find $pwd "$i" | awk -F"." '{print $NF}'` #convert all files to extention 

if [ "$f" != "$f2" ] #compare these extentions if $1 not equal all files ext.
then
k=5                  #k value =5 for only one argument executions.
str1=$i              #
arr[index]=$i
echo ${arr[$index]}
let "index=index+1"
fi
done

#h=${#*}
#echo "$h"
#echo "$*"

if [[ $k -ne 5 ]]  #if k not equal 5 ,execute only one argument 
then
l1=0
for i in $*;       # for loops to calculate all files line in current .
do
    echo "$i"
    l=`grep -c "." $i` #find line number in file
    l1=`expr $l1 + $l` 
    
done
echo "Total is $l1."  #total result for only one arguments.

#### third part two argument exists.
elif [[ $k -eq 5 ]]  #if k equal 5 ,execute for two arguments
then
#echo "s1=$1"
#echo "str=$str1"
#cut last directory from str1 string , str1 is second argument
arrLen=${#arr[@]}
echo "$arrLen"
j=0
while [ "$j" -lt "$arrLen" ]
do
strn=${arr[j]} 
echo "$strn"
r2=$(echo "$strn" | awk -F'/' '{print $NF}') 
echo "r2=$r2"
loc=`locate -b '\'$r2` # find to path of last directory.
echo "loc=>$loc"
#echo "f=$f"
#search all directories.
#loc="/home/kenan/Desktop/courses/cse333"
cur=`find $loc -type f -exec ls -l {} \; | awk '{ print $9  }' | awk -F"/" '{print $NF}'` #bu directordeki bütün dosyaları gez
#echo "$cur"
#search all directories extentions
cur2=`find $loc -type f -exec ls -l {} \; | awk '{ print $9  }' | awk -F"." '{print $NF}'` #bu directordeki bütün dosyaları gez
#echo "cur2=$cur2"

cd $loc
for i in $cur; do
cur3=$(echo "$i" | awk -F'.' '{print $NF}')
echo "cur3=$cur3"
if [ "$f" == "$cur3" ]  #first arg. to extention  equals to all files ext. 
then
    echo "$i"
    l=`grep -c "." $i`  #measure line number in file
    l1=`expr $l1 + $l` 
fi
done
let "j=j+1"
done
echo "Total is $l1."    #Total line for two arguments.
fi

#**********end program********
