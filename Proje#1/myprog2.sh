if [ $# -eq 0 ]                      #warning if no arguments 
then 
echo "Usage $0: you should give ınput" #****if input not exist**
    exit 1
fi

file=`find -name "*$1*"` #search  pattern in files.
if [ -z "$file" ]        #warning if no arguments 
then 
echo "Warning:This pattern is not found." #****Warning this pattern not found
    exit 1
fi
#print all files containing the pattern 
echo "`find $file -type f -exec ls -l --time-style='full-iso' {} \; | awk '{ print "The file " $9 " was modified on "$6 " at " $7"." }'`" | awk -F"/" '{print "The file " $NF}' 
#time styleis  date:year-month-day && time=hour:minute:sec
#awk -F"/": it means filename is not contain "/"
#$9:filename
#$6:date
#$7:time
