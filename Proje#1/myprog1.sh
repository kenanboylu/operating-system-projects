if [ $# -eq 0 ]    #warning if no arguments 
then 
echo "Warning $0: you should give ınput arguments" #****if input not exist**
    exit 1
fi

if [ -z $2 ]       #if we give only one arguments to shell.
then 
#find size and filename that is sorted the largest size in curent directory 
k1=`find $pwd -printf "%s %f\n" | sort -k1rn | head -$1` 
                                              #head is  n times files
echo "$k1"   #print 

    exit 1
fi

#find size and filename that is sorted the largest size in second argument
k2=`find $2 -printf "%s %f\n" | sort -k1rn | head -$1`
                                            #head is  n times files
echo "$k2"  #print


