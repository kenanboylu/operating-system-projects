if [ $# -eq 0 ]    #warning if no arguments 
then 
echo "Usage: ./myprog4.sh <start> <stop> <step>
             ./myprog4.sh <start> <stop>
             ./myprog4.sh <stop>
"
exit 1
fi
start=$1    #start argument
stop=$2     #stop argument
step=$3     #step argument

if [ -z "$step" ] # if there is not third argument 
then              # step equals 1
step=1
fi

if [ -z "$stop" ] # if there is not second argument
then              # stop equals 1
stop=1
fi

if [ "$start" -gt "$stop" ]    # if start greater than stop
then
while [ "$start" -gt "$stop" ] # if start greater than stop 
do                             # control with while always
 echo "$start"                 # print start
 let "start=start-step"        # subract step from start 
done
exit 1

elif [ "$start" -lt "$stop" ]  # if start less than stop
then 
while [ "$start" -lt "$stop" ] # if start less than stop
do                             # control with while always
 echo "$start"                 # print start
 let "start=start+step"        # subract step from start
done
exit 1
fi
